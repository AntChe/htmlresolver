import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.xpath.*;

public class XpathExtractor {

    private Document document;

    private String xpathExp;

    public XpathExtractor() {

    }

    public XpathExtractor(Document doc, String expression) {
        document = doc;
        xpathExp = expression;
    }

    public NamedNodeMap getOrigAttributes() {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        try {
            XPathExpression expr = xpath.compile("//*[@" + xpathExp + "]");
            return ((Node)expr.evaluate(document, XPathConstants.NODE)).getAttributes();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Node getNode(Document document, String xpathExp) {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        try {
            XPathExpression expr = xpath.compile(xpathExp);
            return (Node)expr.evaluate(document, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return null;
    }
}
