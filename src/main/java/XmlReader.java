import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class XmlReader {

    private static final int DEPTH_OF_COMPARING = 2;

    private static final String ID = "id=\"make-everything-ok-button\"";

    private String originalPath;

    private String newPath;

    public XmlReader(String[] args) {
        originalPath = args[0];
        newPath = args[1];
    }

    public String readXmls() {
        NamedNodeMap mapOrig = readOriginalMap(originalPath);
        if (mapOrig != null) {
            return compareAttributtes(newPath, mapOrig);
        }

        return "N/A";
    }

    private NamedNodeMap readOriginalMap(String path) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File(path);
            Document doc = builder.parse(file);

            XpathExtractor extractor = new XpathExtractor(doc, ID);
            return extractor.getOrigAttributes();

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String compareAttributtes(String path, NamedNodeMap map) {
        Document document = parseDoc(path);
        List<Node> nodes = new ArrayList<>();
        if (document != null) {
            XpathExtractor extractor = new XpathExtractor();
            for (int i=0; i<map.getLength(); i++) {
                nodes.add(extractor.getNode(document, "//*[@" + map.item(i).getNodeName() + "=" + "\"" + map.item(i).getNodeValue() + "\"]"));
            }
        }

        return findNode(nodes, map);
    }

    private String findNode(List<Node> nodes, NamedNodeMap map) {
        List<Node> filtered = nodes.stream().filter(node -> node != null)
                .filter(node -> node.getAttributes()
                .getLength() > DEPTH_OF_COMPARING
                && node.getAttributes().getLength() <= map.getLength()).distinct().collect(Collectors.toList());

        return compareNodes(filtered, map);
    }

    private String compareNodes(List<Node> nodes, NamedNodeMap map) {
        int[] counters = new int[nodes.size()];
        for (int i=0; i<nodes.size(); i++) {
            for (int j=0; j<nodes.get(i).getAttributes().getLength(); j++) {
                NamedNodeMap nodeMap = nodes.get(i).getAttributes();
                if (nodeMap.item(j).getNodeValue().equals(map.getNamedItem(nodeMap.item(j).getNodeName()).getNodeValue())) {
                    counters[i]++;
                }
            }
        }

        int indexOfMax = getIndexOfMax(counters);
        return getPath(nodes.get(indexOfMax));
    }

    private String getPath(Node node) {
        Node parent = node.getParentNode();
        if (parent == null) {
            return node.getNodeName();
        }
        return getPath(parent) + "/" +
                node.getAttributes().getNamedItem("class") + "@" + node.getNodeName();
    }

    private int getIndexOfMax(int[] array) {
        int max = 0;
        for (int i = 1; i<array.length; i++) {
            if (array[i] > array[max]) {
                max = i;
            }
        }

        return max;
    }

    private Document parseDoc(String path) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File(path);
            return builder.parse(file);
        }
        catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
